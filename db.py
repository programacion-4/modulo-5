
import redis
class REDIS():
    def __init__(self,host,port,db):
        self.host = host
        self.port = port
        self.db = db
        self.redisdb = redis.Redis(host=self.host,port=self.port,db=self.db)
    def agregarPalabra(self,palabra,definicion):
        data = {'palabra':palabra,'definicion':definicion}
        self.redisdb.hmset('Palabras:'+palabra,data)

    def obtenerPalabra(self,palabra):
        resultado = self.redisdb.hgetall('Palabras:'+palabra)
        return {'id':palabra,list(resultado.keys())[0].decode('utf-8'):resultado[list(resultado.keys())[0]].decode('utf-8'),list(resultado.keys())[1].decode('utf-8'):resultado[list(resultado.keys())[1]].decode('utf-8')}


    def obtenerTodo(self):
        res = self.redisdb.keys('Palabras:*')
        data = []
        for info in res:
            data.append(info.decode('utf-8'))
        result=[]
        for info in data:
            result.append(self.getByKey(info))
        return result

    def editarPalabra(self,palabra,newPalabra):
        data = {'palabra':newPalabra}
        self.redisdb.hmset('Palabras:'+palabra,data)

    def editarDefinicion(self,palabra,definicion):
        data = {'definicion':definicion}
        self.redisdb.hmset('Palabras:'+palabra,data)

    def borrarPalabra(self,palabra):
        self.redisdb.delete('Palabras:'+palabra)
    def getByKey(self,key):
        res = self.redisdb.hgetall(key)
        value = {'id':key[9:]}
        for k,v in res.items():
            value[k.decode('utf-8')]=v.decode('utf-8')
        res = value
        return res
    def obtenerLlaves(self):
        res = self.redisdb.keys('Palabras:*')
        val = []
        for item in res:
            data = item.decode('utf-8') 
            val.append(data[9:]) 
        return val

